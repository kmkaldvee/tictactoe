package com.example.tictactoe;

import android.content.Context;
import android.graphics.Color;
import android.media.Image;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;



public class MainActivity extends AppCompatActivity {

    // mänguväli
int gameBoard [][] = {  {0,0,0},
                        {0,0,0},
                        {0,0,0}

};

    // mängija
    // mängu alustab player 1 = O, 2 = X


    private int scoreP1 = 0;
    private int scoreP2 = 0;
    //nupud
    ImageView B00;
    ImageView B01;
    ImageView B02;
    ImageView B10;
    ImageView B11;
    ImageView B12;
    ImageView B20;
    ImageView B21;
    ImageView B22;
    Button reset;
    TextView playerID;
    TextView p1_points;
    TextView p2_points;
    boolean win;
    boolean draw;

    int activePlayer = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        B00 = findViewById(R.id.B00);
        B01 = findViewById(R.id.B01);
        B02 = findViewById(R.id.B02);
        B10 = findViewById(R.id.B10);
        B11 = findViewById(R.id.B11);
        B12 = findViewById(R.id.B12);
        B20 = findViewById(R.id.B20);
        B21 = findViewById(R.id.B21);
        B22 = findViewById(R.id.B22);
        reset = findViewById(R.id.reset);
        playerID = findViewById(R.id.playerID);
        p1_points = findViewById(R.id.p1_points);
        p2_points = findViewById(R.id.p2_points);

        getSupportActionBar().hide();

        B00.setOnClickListener(new View.OnClickListener() {
            //int activePlayer = 1;
            //nupu B00 funkstionaalsus

            @Override
            public void onClick(View v) {
                if (activePlayer == 1) {
                    Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibe.vibrate(100);
                    B00.setImageResource(R.drawable.ic_x);
                    gameBoard [0][0] = 1;
                    CheckWin();
                    if (!win && !draw) {
                        activePlayer = 2;
                        playerID.setText("Player 2");
                        playerID.setTextColor(Color.argb(255, 0, 107, 184));
                        CheckWin();
                    }


                }

                else if (activePlayer == 2)
                {
                    Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibe.vibrate(100);
                    B00.setImageResource(R.drawable.ic_o);
                    gameBoard [0][0] = 2;
                    CheckWin();
                    if (!win && !draw) {
                        activePlayer = 1;
                        playerID.setText("Player 1");
                        playerID.setTextColor(Color.argb(255, 19, 184, 64));
                        CheckWin();
                    }
                }
                B00.setEnabled(false);
            }
        });

        B01.setOnClickListener(new View.OnClickListener() {
            //nupu B01 funkstionaalsus
            @Override
            public void onClick(View v) {
                if (activePlayer == 1) {
                    Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibe.vibrate(100);
                    B01.setImageResource(R.drawable.ic_x);
                    gameBoard [0][1] = 1;
                    CheckWin();
                    if (!win && !draw) {
                        activePlayer = 2;
                        playerID.setText("Player 2");
                        playerID.setTextColor(Color.argb(255, 0, 107, 184));
                        CheckWin();
                    }
                }

                else if (activePlayer == 2)
                {
                    Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibe.vibrate(100);
                    B01.setImageResource(R.drawable.ic_o);
                    gameBoard [0][1] = 2;
                    CheckWin();
                    if (!win && !draw) {
                        activePlayer = 1;
                        playerID.setText("Player 1");
                        playerID.setTextColor(Color.argb(255, 19, 184, 64));
                        CheckWin();
                    }             }
                B01.setEnabled(false);
            }
        });

        B02.setOnClickListener(new View.OnClickListener() {
            //nupu B02 funkstionaalsus
            @Override
            public void onClick(View v) {
                if (activePlayer == 1) {
                    Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibe.vibrate(100);
                    B02.setImageResource(R.drawable.ic_x);
                    gameBoard [0][2] = 1;
                    CheckWin();
                    if (!win && !draw) {
                        activePlayer = 2;
                        playerID.setText("Player 2");
                        playerID.setTextColor(Color.argb(255, 0, 107, 184));
                        CheckWin();
                    }
                }

                else if (activePlayer == 2)
                {
                    Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibe.vibrate(100);
                    B02.setImageResource(R.drawable.ic_o);
                    gameBoard [0][2] = 2;
                    CheckWin();
                    if (!win && !draw) {
                        activePlayer = 1;
                        playerID.setText("Player 1");
                        playerID.setTextColor(Color.argb(255, 19, 184, 64));
                        CheckWin();
                    }              }
                B02.setEnabled(false);
            }
        });

        B10.setOnClickListener(new View.OnClickListener() {
            //nupu B10 funkstionaalsus
            @Override
            public void onClick(View v) {
                if (activePlayer == 1) {
                    Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibe.vibrate(100);
                    B10.setImageResource(R.drawable.ic_x);
                    gameBoard [1][0] = 1;
                    CheckWin();
                    if (!win && !draw) {
                        activePlayer = 2;
                        playerID.setText("Player 2");
                        playerID.setTextColor(Color.argb(255, 0, 107, 184));
                        CheckWin();
                    }
                }

                else if (activePlayer == 2)
                {
                    Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibe.vibrate(100);
                    B10.setImageResource(R.drawable.ic_o);
                    gameBoard [1][0] = 2;
                    CheckWin();
                    if (!win && !draw) {
                        activePlayer = 1;
                        playerID.setText("Player 1");
                        playerID.setTextColor(Color.argb(255, 19, 184, 64));
                        CheckWin();
                    }             }
                B10.setEnabled(false);
            }
        });

        B11.setOnClickListener(new View.OnClickListener() {
            //nupu B11 funkstionaalsus
            @Override
            public void onClick(View v) {
                if (activePlayer == 1) {
                    Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibe.vibrate(100);
                    B11.setImageResource(R.drawable.ic_x);
                    gameBoard [1][1] = 1;
                    CheckWin();
                    if (!win && !draw) {
                        activePlayer = 2;
                        playerID.setText("Player 2");
                        playerID.setTextColor(Color.argb(255, 0, 107, 184));
                        CheckWin();
                    }

                }

                else if (activePlayer == 2)
                {
                    Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibe.vibrate(100);
                    B11.setImageResource(R.drawable.ic_o);
                    gameBoard [1][1] = 2;
                    CheckWin();
                    if (!win && !draw) {
                        activePlayer = 1;
                        playerID.setText("Player 1");
                        playerID.setTextColor(Color.argb(255, 19, 184, 64));
                        CheckWin();
                    }        }
                B11.setEnabled(false);
            }
        });

        B12.setOnClickListener(new View.OnClickListener() {
            //nupu B12 funkstionaalsus
           @Override
           public void onClick(View v) {
               if (activePlayer == 1) {
                   Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                   vibe.vibrate(100);
                   B12.setImageResource(R.drawable.ic_x);
                   gameBoard [1][2] = 1;
                   CheckWin();
                   if (!win && !draw) {
                       activePlayer = 2;
                       playerID.setText("Player 2");
                       playerID.setTextColor(Color.argb(255, 0, 107, 184));
                       CheckWin();
                   }
               }

               else if (activePlayer == 2)
               {
                   Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                   vibe.vibrate(100);
                   B12.setImageResource(R.drawable.ic_o);
                   gameBoard [1][2] = 2;
                   CheckWin();
                   if (!win && !draw) {
                       activePlayer = 1;
                       playerID.setText("Player 1");
                       playerID.setTextColor(Color.argb(255, 19, 184, 64));
                       CheckWin();
                   }
               }
               B12.setEnabled(false);
           }
        });

        B20.setOnClickListener(new View.OnClickListener() {
            //nupu B20 funkstionaalsus
            @Override
            public void onClick(View v) {
                if (activePlayer == 1) {
                    Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibe.vibrate(100);
                    B20.setImageResource(R.drawable.ic_x);
                    gameBoard [2][0] = 1;
                    CheckWin();
                    if (!win && !draw) {
                        activePlayer = 2;
                        playerID.setText("Player 2");
                        playerID.setTextColor(Color.argb(255, 0, 107, 184));
                        CheckWin();
                    }
                }

                else if (activePlayer == 2)
                {
                    Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibe.vibrate(100);
                    B20.setImageResource(R.drawable.ic_o);
                    gameBoard [2][0] = 2;
                    CheckWin();
                    if (!win && !draw) {
                        activePlayer = 1;
                        playerID.setText("Player 1");
                        playerID.setTextColor(Color.argb(255, 19, 184, 64));
                        CheckWin();
                    }             }
                B20.setEnabled(false);
            }
        });

        B21.setOnClickListener(new View.OnClickListener() {
            //nupu B21 funkstionaalsus
            @Override
            public void onClick(View v) {
                // kui mängija 1 on aktiivne
                if (activePlayer == 1) {
                    Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibe.vibrate(100);
                    B21.setImageResource(R.drawable.ic_x);
                    gameBoard [2][1] = 1;
                    //võidukontroll
                    CheckWin();
                    if (!win && !draw) {
                        activePlayer = 2;
                        playerID.setText("Player 2");
                        playerID.setTextColor(Color.argb(255, 0, 107, 184));
                        CheckWin();
                    }
                }
                // kui mängija 2 on aktiivne
                else if (activePlayer == 2)
                {
                    Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibe.vibrate(100);
                    B21.setImageResource(R.drawable.ic_o);
                    gameBoard [2][1] = 2;
                    //võidukontroll
                    CheckWin();
                    if (!win && !draw) {
                        activePlayer = 1;
                        playerID.setText("Player 1");
                        playerID.setTextColor(Color.argb(255, 19, 184, 64));
                        CheckWin();
                    }
                }
                // deaktiveeri nupp
                B21.setEnabled(false);
            }
        });


        B22.setOnClickListener(new View.OnClickListener() {
            //nupu B22 funkstionaalsus
           @Override
           public void onClick(View v) {
               // kui mängija 1 on aktiivne
               if (activePlayer == 1) {
                   Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                   vibe.vibrate(100);
                   B22.setImageResource(R.drawable.ic_x);
                   gameBoard [2][2] = 1;
                   //võidukontroll
                   CheckWin();
                   if (!win && !draw) {
                       activePlayer = 2;
                       playerID.setText("Player 2");
                       playerID.setTextColor(Color.argb(255, 0, 107, 184));
                   }
               }
               // kui mängija 2 on aktiivne
               else if (activePlayer == 2)
               {
                   Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                   vibe.vibrate(100);
                   B22.setImageResource(R.drawable.ic_o);
                   gameBoard [2][2] = 2;
                   //võidukontroll
                   CheckWin();
                   if (!win && !draw) {
                       activePlayer = 1;
                       playerID.setText("Player 1");
                       playerID.setTextColor(Color.argb(255, 19, 184, 64));
                       CheckWin();
                   }
               }
               // deaktiveeri nupp
               B22.setEnabled(false);
           }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            // int activePlayer = 1;
            //nupu B22 funkstionaalsus
            @Override
            public void onClick(View v) {
                B00.setImageResource(R.drawable.ic_default);
                B01.setImageResource(R.drawable.ic_default);
                B02.setImageResource(R.drawable.ic_default);
                B10.setImageResource(R.drawable.ic_default);
                B20.setImageResource(R.drawable.ic_default);
                B21.setImageResource(R.drawable.ic_default);
                B12.setImageResource(R.drawable.ic_default);
                B22.setImageResource(R.drawable.ic_default);
                B11.setImageResource(R.drawable.ic_default);

                B00.setEnabled(true);
                B01.setEnabled(true);
                B02.setEnabled(true);
                B10.setEnabled(true);
                B20.setEnabled(true);
                B22.setEnabled(true);
                B11.setEnabled(true);
                B12.setEnabled(true);
                B21.setEnabled(true);

                gameBoard [0][0] = 0;
                gameBoard [0][1] = 0;
                gameBoard [0][2] = 0;
                gameBoard [1][0] = 0;
                gameBoard [1][1] = 0;
                gameBoard [1][2] = 0;
                gameBoard [2][0] = 0;
                gameBoard [2][1] = 0;
                gameBoard [2][2] = 0;

                activePlayer = 1;
                win = false;
                draw = false;
                playerID.setText("Player 1");
                playerID.setTextColor(Color.argb(255,19,184,64));            }
        });
    }

    private void disableButtons() {
        B01.setEnabled(false);
        B02.setEnabled(false);
        B00.setEnabled(false);
        B10.setEnabled(false);
        B20.setEnabled(false);
        B22.setEnabled(false);
        B11.setEnabled(false);
        B12.setEnabled(false);
        B21.setEnabled(false);
    }

    private void CheckWin() {

        if (gameBoard[0][0] == activePlayer && gameBoard[0][1] == activePlayer && gameBoard[0][2] == activePlayer){
            disableButtons();
            win = true;
        }
        else if (gameBoard[1][0] == activePlayer && gameBoard[1][1] == activePlayer && gameBoard[1][2] == activePlayer) {
            disableButtons();
            win = true;
        }
        else if (gameBoard[2][0] == activePlayer && gameBoard[2][1] == activePlayer && gameBoard[2][2] == activePlayer) {
            disableButtons();
            win = true;
        }
        else if (gameBoard[0][0] == activePlayer && gameBoard[1][0] == activePlayer && gameBoard[2][0] == activePlayer) {
            disableButtons();
            win = true;
        }
        else if (gameBoard[0][1] == activePlayer && gameBoard[1][1] == activePlayer && gameBoard[2][1] == activePlayer) {
            disableButtons();
            win = true;
        }
        else if (gameBoard[0][2] == activePlayer && gameBoard[1][2] == activePlayer && gameBoard[2][2] == activePlayer) {
            disableButtons();
            win = true;
        }
        else if (gameBoard[0][0] == activePlayer && gameBoard[1][1] == activePlayer && gameBoard[2][2] == activePlayer) {
            disableButtons();
            win = true;
        }
        else if (gameBoard[0][2] == activePlayer && gameBoard[1][1] == activePlayer && gameBoard[2][0] == activePlayer) {
            disableButtons();
            win = true;
        }
        //kontrolli viiki
        else if (gameBoard[0][0] != 0 && gameBoard[0][1] != 0 &&gameBoard[0][2] != 0 && gameBoard[1][0] != 0 && gameBoard[2][0] != 0
                && gameBoard[1][2] != 0 && gameBoard[2][2] != 0 && gameBoard[2][1] != 0 && gameBoard[1][1] != 0){
            playerID.setTextColor(Color.argb(255, 0, 0, 0));
            playerID.setText("DRAW");
            draw = true;
        }

        if (win == true) {

            if(activePlayer == 1){
                playerID.setTextColor(Color.argb(255, 19, 184, 64));
                scoreP1++;
                playerID.setText("WIN");

            }
            else {
                playerID.setTextColor(Color.argb(255, 0, 107, 184));
                scoreP2++;
                playerID.setText("WIN");
            }

            p1_points.setText("P1: " + Integer.toString(scoreP1));
            p2_points.setText("P2: " + Integer.toString(scoreP2));

        }

    }

}
