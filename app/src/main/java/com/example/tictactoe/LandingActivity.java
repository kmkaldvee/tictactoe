package com.example.tictactoe;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class LandingActivity extends AppCompatActivity {

    Button twoPlayer;
    Button onePlayer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        getSupportActionBar().hide();

        twoPlayer = findViewById(R.id.twoPlayer);
        onePlayer = findViewById(R.id.onePlayer);

        twoPlayer.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent toTwoPlayer = new Intent (LandingActivity.this, MainActivity.class);
                LandingActivity.this.startActivity(toTwoPlayer);

                }

            });

        onePlayer.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent toTwoPlayer = new Intent (LandingActivity.this, OnePlayerActivity.class);
                LandingActivity.this.startActivity(toTwoPlayer);

            }

        });


        }
    }


